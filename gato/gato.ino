#include <Keypad.h>
#include "BluetoothSerial.h"
//#include "Box.h"

BluetoothSerial SerialBT;
 
const byte rowsCount = 3;
const byte columnsCount = 3;
 
char keys[rowsCount][columnsCount] = {
   { '0','1','2' },
   { '3','4','5' },
   { '6','7','8' }
};
 
byte rowsPins[rowsCount] = { 13, 12, 14 };
byte columnsPins[columnsCount] = { 27, 26, 25 };
 
Keypad keypad = Keypad(makeKeymap(keys), rowsPins, columnsPins, rowsCount, columnsCount);

//Box *boxes[] = {
//  new Box(36, 19),
//  new Box(39, 18),
//  new Box(34, 5),
//  new Box(35, 17),
//  new Box(32, 16),
//  new Box(33, 4),
//  new Box(23, 2),
//  new Box(22, 15),
//  new Box(21, 0)
//};

bool turn = true;
int t1 = 36;
int t2 = 39;
int t3 = 34;
int t4 = 35;
 
void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32-Dsk8"); //Bluetooth device name

  pinMode(t1, OUTPUT);
  pinMode(t2, OUTPUT);
  pinMode(t3, OUTPUT);
  pinMode(t4, OUTPUT);
}
 
void loop() {
  char key = keypad.getKey();
  
  if (key) {
    Serial.write(key);
//    boxes[key]->setMark(turn);
//    turn = !turn;
    
    digitalWrite(t1, HIGH);
  }
  if (SerialBT.available()) {
    Serial.write(SerialBT.read());
  }
  delay(20);
}
